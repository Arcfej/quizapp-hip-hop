package com.example.android.quizapp_hip_hop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import static java.util.Arrays.asList;

public class MainActivity extends AppCompatActivity {

    // The number of the correctly answered questions
    int correctAnswers;
    // The number of the next question
    int questionNumber;
    // The number of all the questions in the quiz
    int allQuestions;
    // The number of styles which have to be sorted.
    int sortTextNumber;
    // The layout which a button click will hide
    View layoutBefore;
    // The layout which a button click will show
    View layoutAfter;
    ScrollView scrollRoot;
    String[] hipHopStyles;
    String[] nonHipHopStyles;
    String[] styles70sArray;
    String[] styles80sArray;
    String[] styles00sArray;
    ArrayList<String> allStyles = new ArrayList<>();

    // TODO solve screen orientation
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutBefore = findViewById(R.id.layout_result);
        scrollRoot = (ScrollView) findViewById(R.id.scroll_view_root);
        ViewGroup linearLayoutRoot = (ViewGroup) findViewById(R.id.linear_layout_root);
        allQuestions = linearLayoutRoot.getChildCount() - 2;
        sortTextNumber = ((ViewGroup) findViewById(R.id.relative_layout_styles_for_sort)).getChildCount() / 2;
        // Create the style sorting question's question text with the calculated sortTextNumber
        ((TextView) findViewById(R.id.question_sort_styles_text)).setText(getResources().getString(R.string.question_title_sort_styles, sortTextNumber));
        // Create 2 string arrays from strings.xml and add them into one ArrayList
        hipHopStyles = getResources().getStringArray(R.array.hip_hop_styles);
        nonHipHopStyles = getResources().getStringArray(R.array.non_hip_hop_styles);
        allStyles.addAll(asList(hipHopStyles));
        allStyles.addAll(asList(nonHipHopStyles));
        styles70sArray = getResources().getStringArray(R.array.styles_of_70s);
        styles80sArray = getResources().getStringArray(R.array.styles_of_80s);
        styles00sArray = getResources().getStringArray(R.array.styles_of_00s);
        // Set up the program to starting position
        toStartingPosition();
    }

    /**
     * Set the necessary variables and objects to the program's starting position.
     * Also set up the visible layouts.
     */
    private void toStartingPosition() {
        correctAnswers = 0;
        questionNumber = 0;
        layoutAfter = findViewById(R.id.layout_intro);
        changeLayout();
        // Shuffle the list of allStyles. It adds some randomness to it.
        Collections.shuffle(allStyles);
        // Count the number of the CheckBoxes
        int checkBoxNumber = ((ViewGroup) findViewById(R.id.parent_of_checkboxes)).getChildCount() - 1;
        // Set texts to the checkboxes from allStyles
        setCheckboxTexts(allStyles, "checkbox_style", checkBoxNumber);
        // Set the texts for the styles next to the editTexts
        String styles70sText = styles70sArray[(int) (Math.random() * styles70sArray.length)];
        String styles80sText = styles80sArray[(int) (Math.random() * styles80sArray.length)];
        String styles00sText = styles00sArray[(int) (Math.random() * styles00sArray.length)];
        ((TextView) findViewById(R.id.text_sort1)).setText(styles80sText);
        ((TextView) findViewById(R.id.text_sort2)).setText(styles00sText);
        ((TextView) findViewById(R.id.text_sort3)).setText(styles70sText);
        // Empty the editTexts, uncheck all checkboxes and radio buttons.
        ((EditText) findViewById(R.id.edit_text_group_name)).setText("");
        for (int i = 0; i < sortTextNumber; i++) {
            ((EditText) findIndexedView("edit_text_sort", i + 1)).setText("");
        }
        for (int i = 0; i < checkBoxNumber; i++) {
            ((CheckBox) findIndexedView("checkbox_style", i + 1)).setChecked(false);
        }
        ((RadioGroup) findViewById(R.id.radio_group_dj)).clearCheck();
    }

    /**
     * This method triggers when the START button clicked.
     * It hides the first layout and shows the next one.
     */
    public void startQuiz(View view) {
        changeLayout();
    }

    /**
     * This method evaluate the first question and switches to the next one.
     * It's triggered when the NEXT button clicked at the first question.
     */
    public void evaluateFirstQuestion(View view) {
        EditText text = (EditText) findViewById(R.id.edit_text_group_name);
        String group = text.getText().toString();
        if (group.equals("The Lockers") || group.equals("Lockers")) {
            correctAnswers += 1;
        }
        changeLayout();
    }

    /**
     * This method evaluate the second question and switches to the next one.
     * It's triggered when the NEXT button clicked at the second question.
     */
    public void evaluateSecondQuestion(View view) {
        for (int i = 0; i < 10; i++) {
            CheckBox checkbox = (CheckBox) findIndexedView("checkbox_style", i + 1);
            String text = checkbox.getText().toString();
            // Exit the method if any of the checks is incorrect
            if (asList(hipHopStyles).contains(text)) {
                if (!checkbox.isChecked()) {
                    changeLayout();
                    return;
                }
            } else {
                if (checkbox.isChecked()) {
                    changeLayout();
                    return;
                }
            }
        }
        correctAnswers += 1;
        changeLayout();
    }

    // TODO randomize the radio button sequence. Has to change the radioButtons IDs, and randomize the texts in the toStartingPosition method.
    // TODO The question evaluate checks if the checked radioButton's text equals to "DJ Kool Herc"

    /**
     * This method evaluates the third question and switches to the next one.
     * It's triggered when the NEXT button clicked at the third question.
     */
    public void evaluateThirdQuestion(View view) {
        RadioGroup group = (RadioGroup) findViewById(R.id.radio_group_dj);
        if (group.getCheckedRadioButtonId() == R.id.radio_kool_herc) {
            correctAnswers += 1;
        }
        changeLayout();
    }

    /**
     * This Method evaluates the fourth question and switches to the next one.
     * It's triggered when the NEXT button clicked at the fourth question.
     */
    public void evaluateFourthQuestion(View view) {
        String number1 = ((TextView) findViewById(R.id.edit_text_sort1)).getText().toString();
        String number2 = ((TextView) findViewById(R.id.edit_text_sort2)).getText().toString();
        String number3 = ((TextView) findViewById(R.id.edit_text_sort3)).getText().toString();
        if (number1.equals("2") && number2.equals("3") && number3.equals("1")) {
            correctAnswers += 1;
        }
        changeLayout();
    }

    /**
     * This method restart the quiz.
     * It's triggerd, when the RESTART button clicked.
     */
    public void restartQuiz(View view) {
        toStartingPosition();
    }

    /**
     * This method hides the currently displayed layout and shows the next one.
     * Than prepare the program for the next layout change by setting the objects: layoutBefore and layoutAfter
     * If the last question was the final one it's also evaluate the quiz.
     * Always set the root scrollView scroll position to the top.
     */
    private void changeLayout() {
        if (questionNumber > allQuestions) {
            layoutAfter = findViewById(R.id.layout_result);
            finalEvaluation();
        }
        layoutBefore.setVisibility(View.GONE);
        layoutAfter.setVisibility(View.VISIBLE);
        layoutBefore = layoutAfter;
        questionNumber += 1;
        layoutAfter = findIndexedView("layout_question", questionNumber);
        scrollRoot.scrollTo(0, 0);
    }

    /**
     * Calculate the result of the quiz. If you answered at least half of the questions correctly, it's a success.
     */
    private void finalEvaluation() {
        String message;
        String result;
        TextView outcome = (TextView) findViewById(R.id.text_result);
        if (correctAnswers >= (allQuestions / 2.0)) {
            message = getResources().getString(R.string.result_success);
        } else {
            message = getResources().getString(R.string.result_failed);
        }
        result = getResources().getString(R.string.result_answers, correctAnswers, allQuestions);
        TextView congratulation = (TextView) findViewById(R.id.text_congratulations);
        congratulation.setText(message);
        outcome.setText(result);
        // The task requires a toast message
        Toast evaluation = Toast.makeText(this, message + "\n" + result, Toast.LENGTH_SHORT);
        evaluation.setGravity(Gravity.CENTER, 0, 0);
        evaluation.show();
    }

    /**
     * Example:
     * There is a set of view. Their IDs are view1, view2, view3
     * This method concatenates the given name and number (in the example it's (view + 2) )
     * then find the View by the id (view2) and return it.
     *
     * @param viewName is the first part of the View's ID.
     * @param index    is the end of the View's ID. It's a number.
     * @return the desired View
     */
    private View findIndexedView(String viewName, int index) {
        String layout_id = viewName + index;
        return findViewById(getResources().getIdentifier(layout_id, "id", getPackageName()));
    }

    /**
     * Set the texts of a list of checkboxes
     *
     * @param list       is the list of texts
     * @param CheckboxId the first part of an indexed View Id without the number at the end
     * @param number     the number of the checkboxes
     */
    private void setCheckboxTexts(ArrayList<String> list, String CheckboxId, int number) {
        for (int i = 0; i < number; i++) {
            String checkboxText;
            CheckBox checkboxView;
            checkboxText = list.get(i);
            checkboxView = (CheckBox) findIndexedView(CheckboxId, i + 1);
            checkboxView.setText(checkboxText);
        }
    }
}
